## Les Diagrammes en UML

Le langage UML est une standardisation de différents diagrammes de conceptions qui peuvent être utilisés
à différentes étapes du développement d'un produit. Ces diagrammes permettent une communication efficace
et claire entre les différents acteur du développement (client, développeurs, designer ...), ce qui essentiel
pour n'importe quel projet d'envergure, notamment les applications mobiles