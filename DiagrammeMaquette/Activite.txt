    ##Diagramme d’activité

Le diagramme d’activité permet de représenter graphiquement le “workflow” de certaines activités ou fonctionnalités,
en représentant les différents états du système par des colonnes sur le diagramme. Les transitions entre états sont ensuite modélisées
par un ensemble d'actions et de décisions qui constituent le workflow du système.

##Légende du diagramme :
Rectangle arrondis : Actions
Losanges : Décisions
Rectangles : "Produits" émis
Barres : Début ou fin d’un bloc d’activités parallèles
Rond noir : Etat initial
Rond noir entouré : Etat final