# GITHUB vs GITLAB

## Commandes identiques.. mais différentes

| GITHUB | GITLAB | Signification |
|--------|--------|---------------|
|Pull request | Merge request | Demande de fusion de branches |
|--------|--------|---------------|
| Gist | Snippet | Petit morceau de code |
|--------|--------|---------------|
| Repository | Project | Conteneur accueillant le référentiel, les pièces jointes et paramètres du projet |
|--------|--------|---------------|
| Organization | Group | Niveau auquel s’effectue l’attribution des projets aux utilisateurs |

## Différences majeures

GitHub 	GitLab
| Les issues peuvent être suivies dans plusieurs repositories |	Les issues ne peuvent pas être suivies dans plusieurs repositories |
|--------|--------|
| Repositories privés payants | Repositories privés gratuits |
|--------|--------|
| Pas d’hébergement gratuit sur un serveur privé | Hébergement gratuit possible sur un serveur privé |
|--------|--------|
| Intégration continue uniquement avec des outils tiers (Travis CI, CircleCI, etc.) | Intégration continue gratuite incluse |
|--------|--------|
| Aucune plateforme de déploiement intégrée |	Déploiement logiciel avec Kubernetes |
|--------|--------|
| Suivi détaillé des commentaires |	Pas de suivi des commentaires|
|--------|--------|
| Impossible d’exporter les issues au format CSV |	Exportation possible des issues au format CSV par e-mail|
|--------|--------|
| Tableau de bord personnel pour suivre les issues et pull requests | Tableau de bord analytique pour planifier et surveiller le projet |