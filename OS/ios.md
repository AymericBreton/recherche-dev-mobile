##### Etymologie

iOS, anciennement iPhone OS, le « i » d'iOS étant pour iPhone d'où la minuscule, est le système d'exploitation mobile développé par Apple pour plusieurs de ses appareils. Il est dérivé de macOS dont il partage les fondations (le noyau hybride XNU basé sur le micro-noyau Mach, les services Unix et Cocoa, etc.). iOS comporte quatre couches d'abstraction, similaires à celles de macOS : une couche « Core OS », une couche « Core Services », une couche « Media » et une couche « Cocoa »2,Note 1. Le système d'exploitation occupe au maximum 3 Go de la capacité mémoire totale de l'appareil, selon l'appareil.


##### Histoire

En 2005, quand Steve Jobs a commencé à travailler sur l'iPhone, il s'est donné deux choix : « réduire le Mac, ce qui serait un exploit épique pour l'ingénierie, ou agrandir l'iPod ». Il s'est tourné vers la première approche. Il a alors organisé une compétition entre les équipes Macintosh, dirigée par Scott Forstall, et iPod, dirigée par Tony Fadell. Forstall a alors gagné en créant iPhone OS. Cette décision a donc permis le succès de l'iPhone en tant que plate-forme pour les développeurs tiers, ce qui permettait aux développeurs Mac de travailler sur des applications iPhone plus facilement. Forstall était aussi responsable de la création d'un kit de développement pour construire des applications iPhone mais aussi de l'App Store dans iTunes12,13.

Le système d'exploitation a été révélé avec le premier iPhone à la Macworld Conference & Expo le 9 janvier 2007, sa sortie était prévue en juin de la même année14,15,16.

L'App Store d'iOS a ouvert le 10 juillet 2008 avec 500 applications disponibles17. Nombre qui a augmenté rapidement à 3 000 en septembre 200818, 15 000 en janvier 2009, 50 000 en juin 2009, 100 000 en novembre 2009, 250 000 en août 2010, 650 000 en juillet 2012, un million en octobre 2013, 2 millions en juin 201619 et 2,2 million en janvier 2017. En mars 2016, un million d'applications étaient compatibles avec l'iPad20.

En septembre 2007, Apple a annoncé l'iPod Touch, un iPod basé sur le form factor de l'iPhone21. En janvier 2010, Apple annonce l'iPad, avec un plus grand écran que l'iPhone et l'iPod Touch, élaboré pour la navigation internet, le multimédia et la lecture22.

Lors de la WWDC 2019, Apple annonce qu’un nouveau nom sera désormais utilisé pour qualifier iOS sur iPad : il s’agit d’iPadOS. Après avoir fait naître sur l’iPad la possibilité d’utiliser plusieurs applications en simultané dans iOS 9, ouvert l’iPad au stylet avec l’iPad Pro en 2015, Apple choisit donc de distinguer les OS de l’iPhone et de l’iPad.

##### Applications

Le support d'application utilisé sur l'iPhone et l'iPod touch est basé sur une architecture ARM contrairement aux processeurs utilisés sur les anciennes versions des ordinateurs Apple (PowerPC) ou aux récents (Intel x86). De plus, iOS utilise l'API OpenGL ES tournant sur une carte graphique 3D double cœurs PowerVR. En somme, les applications développées sous macOS ne peuvent pas fonctionner sur un iPhone ou un iPod touch, toutes les applications natives sont re-développées spécifiquement pour l'architecture ARM et les composants logiciels d'iOS.

iOS, à l'achat, comporte une vingtaine d'applications disponibles par défaut, toutes développées par Apple. Leur nombre peut varier légèrement selon l'appareil en question, en raison des différences matérielles mineures qui séparent les cinq appareils disposant de ce système d'exploitation, et disponibles à ce jour. La plupart des applications natives ont été réalisées dans le but de travailler ensemble, permettant ainsi de communiquer intelligemment entre elles. Par exemple, un numéro de téléphone peut être sélectionné au sein d'un courriel et sauvegardé dans le répertoire.

De plus, l'une de ces applications par défaut donne accès, via une connexion Internet, à la plate-forme de téléchargement App Store, qui permet d'ajouter à l'appareil des applications supplémentaires développées par des tiers, et validées par Apple.

Contrairement à certains concurrents, l'iOS n'autorise pas l'exécution d'une application de tierce partie en tâche de fond. Il est cependant multitâche pour certaines de ses applications natives, il est par exemple possible d'écouter de la musique avec l'application iPod en naviguant sur internet avec l'application Safari. Cette lacune est toutefois en partie comblée désormais, avec l'arrivée de la version 3.0 qui inclut un système de notifications envoyées depuis le serveur d'Apple. De plus, la version 4, publiée le 24 juin 2010, a supprimé en partie cette restriction en permettant à certaines API, telle que la musique, de tourner en tâche de fond.

##### API

Voici une liste non exhaustive des API principales contenues dans le SDK, classées par couche d'abstraction :

Core OS, couche la plus « profonde », contient les bases du système d'exploitation :
Le noyau d'OS X,
« Power Management », gestion de la charge du processeur en fonction de la batterie,
« Lib System », le système de bibliothèques,
« KeyChain », les chaînes de clés,
Le protocole TCP/IP du noyau BSD,
La prise en charge des certificats,
La prise en charge des sockets réseau,
Le système de sécurité,
Bonjour
Core Services propose des API de plus haut niveau, permettant une gestion plus poussée du système :
La gestion des collections,
Core Location, qui permet la géolocalisation de l'appareil,
La gestion d'un carnet d'adresse,
« Net Services », des services réseau,
La gestion du réseau en lui-même,
Le « threading », gestion des processus légers,
L'accès à des fichiers,
La gestion de préférences,
SQLite, bibliothèque permettant la gestion de bases de données,
Des utilitaires pour la gestion d'URL
La couche Media gère quant à elle les données multimédia. Son contenu est accéléré matériellement pour de meilleures performances et une meilleure durée de batterie :
Core Audio,
Prise en charge des formats d'image JPEG, PNG, TIFF,
OpenAL,
Prise en charge du format PDF,
La gestion de l'« audio mixing »,
Le moteur graphique Quartz,
La gestion de l'enregistrement audio,
Core Animation,
La gestion du « Video Playback »,
La prise en charge d'OpenGL ES
Cocoa Touch est une réécriture de l'interface graphique Cocoa d'OS X, adaptée cette fois à l'interface Multi-touch d'iOS :
La gestion des événements Multi-touch,
La gestion des alertes,
Les contrôles Multi-touch,
Une vue Web utilisant le moteur de rendu WebKit de Safari,
La gestion de l'accéléromètre,
La sélection d'un contact,
La gestion de contenu avec une hiérarchie,
La sélection d'une image,
La localisation,
L'utilisation de l'appareil photo pour l'iPhone, l'iPhone 3G et l'iPhone 3GS.