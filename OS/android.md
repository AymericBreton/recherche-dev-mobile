﻿Système d'exploitation mobile Android

- ![](Aspose.Words.11f2ddd5-8c03-4fc1-b62c-d4a8cc14b53b.001.png)le noyau Linux avec les pilotes.
- des bibliothèques logicielles telles que WebKit, OpenGL, SQLite ou FreeType.
- une machine virtuelle et des bibliothèques permettant d'exécuter des programmes prévus pour la [plate-forme Java](http://fr.wikipedia.org/wiki/Plate-forme_Java "Plate-forme Java").
- un framework - kit de développement d'applications.
- un lot d'applications standard parmi lesquelles il y a un environnement de bureau, un carnet d'adresses, un navigateur web et un téléphone.

Les services offerts par Android facilitent notamment l'exploitation des réseaux de télécommunications [GSM](http://fr.wikipedia.org/wiki/Global_System_for_Mobile_Communications "Global System for Mobile Communications"), [bluetooth](http://fr.wikipedia.org/wiki/Bluetooth "Bluetooth"), [Wifi](http://fr.wikipedia.org/wiki/Wifi "Wifi") et [UMTS](http://fr.wikipedia.org/wiki/UMTS "UMTS"), la manipulation de médias, notamment de la vidéo [H.264](http://fr.wikipedia.org/wiki/H.264 "H.264"), de l'audio [MP3](http://fr.wikipedia.org/wiki/MPEG-1/2_Audio_Layer_3 "MPEG-1/2 Audio Layer 3") et des images [jpeg](http://fr.wikipedia.org/wiki/Jpeg "Jpeg") ainsi que d'autres formats, 

l'exploitation des senseurs tels que les capteurs de mouvements, la caméra, la boussole et le récepteur GPS, l'utilisation de l'écran tactile, le stockage en base de données, le rendu d'images en 2D ou 3D en utilisant le processeur graphique, l'affichage de page web, l'exécution multitâche des applications et l'envoi de messages SMS7,6.

Bien que ne faisant pas partie de la pile de logiciels, l'environnement de développement qui comporte un émulateur de téléphone et un plugin pour Eclipse peut aussi être considéré comme une fonctionnalité d'Android6. Et Google Play, un site web d'achat et de téléchargement d'applications pour Android, joue un rôle essentiel pour la popularité de ce système d'exploitation7.

Android est distribué en open source sous licence Apache. La licence autorise les constructeurs qui intègrent Android dans leurs appareils à y apporter des modifications leur permettant de se distinguer de leurs concurrents7 et il a été adopté par de nombreux constructeurs de produits concurrents du iPhone7

Le noyau Linux est utilisé pour les fondations d'Android, pour les services classiques des systèmes d'exploitation: utilisation des périphériques, accès aux réseaux de télécommunication, manipulation de la mémoire et des processus et contrôle d'accès. Il s'agit d'une branche du noyau Linux 2.6, modifiée en vue de son utilisation sur des appareils mobiles. De cette branche ont été retirés le X Window System, les outils de GNU, ainsi que plusieurs fichiers de configuration qui se trouvent d'ordinaire dans les distributions Linux. L'équipe de développement d'Android a apporté de nombreuses améliorations au noyau Linux, et la décision a été prise par la communauté de développement de Linux d'incorporer ces améliorations dans le noyau Linux 3.38.

Android et la plateforme Java

Android comporte une machine virtuelle nommée Dalvik, qui permet d'exécuter des programmes prévus pour la plate-forme Java. C'est une machine virtuelle conçue dès le départ pour les appareils mobiles et leurs ressources réduites - peu de puissance de calcul et peu de mémoire6. En effet les appareils mobiles contemporains de 2011 ont la puissance de calcul d'un ordinateur personnel vieux de dix ans9. La majorité, voire la totalité des applications est exécutée par la machine virtuelle Dalvik9.

Le bytecode de Dalvik est différent de celui de la machine virtuelle Java de Oracle (JVM), et le processus de construction d'une application est différent : le code source de l'application, en langage Java est tout d'abord compilé avec un compilateur standard qui produit du bytecode pour JVM (bytecode standard de la plateforme Java) puis ce dernier est traduit en bytecode pour Dalvik par un programme inclus dans Android, du bytecode qui pourra alors être exécuté6.

L'ensemble de la bibliothèque standard de Android ressemble à J2SE (Java Standard Edition) de la plateforme Java. La principale différence est que les bibliothèques d'interface graphique AWT et Swing sont remplacées par des bibliothèques d'Android6.

Le développement d'applications pour Android s'effectue avec un ordinateur personnel sous Mac OS, Windows ou Linux en utilisant le JDK de la plate-forme Java et des outils pour Android. Des outils qui permettent de manipuler le téléphone ou la tablette, de la simuler par une machine virtuelle, de créer des fichiers APK - les fichiers de paquet d'Android, de déboguer les applications et d'y ajouter une signature numérique. Ces outils sont mis à disposition sous la forme d'un plugin pour l'environnement de développement Eclipse7.

La bibliothèque d'Android permet la création d'interfaces graphiques selon un procédé similaire aux frameworks de quatrième génération que sont XUL, JavaFX ou Silverlight: l'interface graphique peut être construite par déclaration et peut être utilisée avec plusieurs skins - chartes graphiques. La programmation consiste à déclarer la composition de l'interface dans des fichiers XML; la description peut comporter des ressources - des textes et des pictogrammes. Ces déclarations sont ensuite transformées en objets tels que des fenêtres et des boutons, qui peuvent être manipulés par de la programmation Java9. Les écrans ou les fenêtres (activités dans le jargon d'Android), sont remplis de plusieurs vues; chaque vue étant une pièce d'interface graphique (bouton, liste, case à cocher…). Android 3.0, destiné aux tablettes, introduit la notion de fragments: des panneaux contenant plusieurs éléments visuels. Une tablette ayant - contrairement à un téléphone - généralement suffisamment de place à l'écran pour plusieurs panneaux9.

Appel à la communauté open source

Dans le message vidéo de présentation d'Android, Sergueï Brin a indiqué que Google comptait sur la communauté des développeurs open source pour créer un système d'exploitation vraiment novateur grâce au SDK disponible à l'adresse http://developer.android.com/ . Il a aussi indiqué qu'une prime globale de 10 millions de dollars28 serait attribuée aux développeurs des meilleures applications dans le cadre de l'Android Developer Challenge.

Le 12 mai 2008, Google annonce la liste des 50 applications retenues dans le cadre de ce concours. Ces 50 applications font pour la plupart appel à des services de géolocalisation et de réseaux sociaux. Tous les participants se voient alors remettre la somme de 25 000 dollars par projet afin de poursuivre le développement. La deuxième partie du défi est alors lancée dans le but de retenir 20 projets finaux en les finançant à hauteur de 275 000 dollars pour les dix premiers et 100 000 dollars pour les dix autres. Une vidéo de présentation d'Android29 fut mise en ligne le 23 octobre 2008, jour de la publication du code source par Google30 et la liste des applications gagnantes du premier Android Developer Challenge31 a été publiée par Google.

Les soumissions du deuxième Android Developer Challenge ont pris fin le 31 août 2009 et les résultats finaux ont été dévoilés en novembre 2009.

Version 

![](Aspose.Words.11f2ddd5-8c03-4fc1-b62c-d4a8cc14b53b.002.png)
## Utilisation
![](Aspose.Words.11f2ddd5-8c03-4fc1-b62c-d4a8cc14b53b.003.png)

L'écran d'accueil d'Android 4.0

Après l'introduction d'un code personnel (exemple: code PIN), les appareils équipés d'Android affichent un écran d'accueil qui sert de point de départ à partir duquel l'utilisateur peut accéder aux applications. Le contenu de cet écran peut être librement personnalisé par l'utilisateur et est souvent personnalisé par les fabricants[\[réf. nécessaire\]](http://fr.wikipedia.org/wiki/Aide:R%C3%A9f%C3%A9rence_n%C3%A9cessaire "Aide:Référence nécessaire"); il se comporte comme une pile de feuilles qui peuvent être glissées l'une sur l'autre avec le doigt. En haut de l'écran d'accueil se trouvent différents icônes d'avertissement et d'état qui permettent par exemple de contrôler le niveau de la batterie et la disponibilité des réseaux[32](http://fr.wikipedia.org/wiki/Android#cite_note-user_guide-32).

Depuis cet écran d'accueil, une icône permet à l'utilisateur d'accéder à l'écran de *launcher*: sur cet écran est affiché la liste de toutes les applications installées dans l'appareil, que l'utilisateur pourra éventuellement ajouter à l'écran d'accueil. Lorsqu'une application est exécutée, l'écran de l'application sera affiché en lieu et place de l'écran d'accueil et un bouton permet à l'utilisateur d'y revenir[32](http://fr.wikipedia.org/wiki/Android#cite_note-user_guide-32).

Les gestes reconnus par l'[écran tactile](http://fr.wikipedia.org/wiki/%C3%89cran_tactile "Écran tactile") des appareils Android sont: toucher l'écran, enfoncer (toucher et maintenir le doigt en contact avec l'écran), déplacer (enfoncer, puis déplacer le doigt en restant en contact avec l'écran), glisser (déplacer le doigt en contact avec l'écran sans s'arrêter), double frappe (toucher deux fois de suite un dessin à l'écran dans 

un délai très court), pincer (mettre deux doigts en contact avec l'écran, puis les rapprocher) et tourner l'écran (changer la position du téléphone, le poser sur le dessus ou sur le coté)32.

Lorsque l'utilisateur doit entrer un texte, Android affiche en bas de l'écran le clavier virtuel - une simulation d'un clavier d'ordinateur. Ce clavier comporte une fonction de copier-coller ainsi que de reconnaissance vocale - qui permet à l'utilisateur de dicter le texte par oral plutôt que de se servir des touches32.


