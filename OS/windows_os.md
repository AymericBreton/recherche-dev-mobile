﻿**Microsoft Windows**

**Windows** (littéralement « Fenêtres » en anglais) est au départ une interface graphique unifiée

produite par Microsoft, qui est devenue ensuite une gamme de systèmes d’exploitation à part entière,

principalement destinés aux ordinateurs compatibles PC.

DOS, que ce soit dans sa version IBM PC-DOS ou dans les versions pour clones MS-DOS, ne comportait pas

d'interface graphique. Il était possible avec une grande facilité de créer des graphiques sous le langage BASICA

(GW-BASIC pour les clones) livré avec le système, mais les commandes devaient être mémorisées par l'utilisateur

et tapées à la main, ce qui rendait le système pénible d'emploi.

Par ailleurs, chaque couple application/périphérique exigeait son pilote générique, ce qui rendait la gestion de ces

pilotes compliquée et constituait un frein à l'évolution des configurations.

Inspirées d'interfaces comme celles du Xerox Alto, puis du Apple Lisa et du Macintosh d'Apple, les premières

versions de Windows, en 16 bits, s'appuyaient sur l'OS existant : MS-DOS. Celui-ci ayant été conçu monotâche,

on y lançait Windows comme un simple programme, qui incorporait dès lors quelques-unes de ses fonctions

(comme le tracking de la souris au système). La limitation intrinsèque propre au monotâche, ainsi que le côté

marginal de Windows 1 (dont les fenêtres ne faisaient que partager l'écran sans superpositions) n'inquiétèrent pas

alors le rival Apple, plus préoccupé de la stratégie d'IBM .

IBM ne pensait pas l'usage du mode graphique viable avec la limitation à 640 K du DOS ni la faible résolution

des écrans de l'époque et s'orienta vers un multi-fenêtrage en mode texte, Topview, très réactif, mais gardant

l'inconfort du DOS.

La version 2 de Windows (1987) déclencha de la part d'Apple un procès pour contrefaçon. Mais Apple le perdit (en appel) à cause du précédent de l'Alto (contre

Digital Research). Apple continua tout de même de menacer Microsoft, ce qui aboutit en 1997 à un règlement à l'amiable : Microsoft produirait Office et Internet

Explorer pour Mac OS et prendrait une part des actions Apple à hauteur de 6% .

Sorti en 1990, Windows 3 intégra trois versions livrées simultanément : une en mode 8086 (16 bits simples), une seconde en mode 80286 (16 bits avec adressage

étendu) et une troisième en mode 80386 (adressage 32 bits). L'appel de la commande *win* depuis le DOS effectuait quelques tests système et lançait

automatiquement la version jugée la plus appropriée, sauf demande expresse de l'utilisateur au moyen de paramètres. La version du DOS était elle aussi testée, afin

de substituer autant de fonctions Windows que possible à celle du DOS, qui ne servait plus guère que de lanceur et d'implanter les vecteurs d'appel aux bons

endroits (un effet collatéral fut une série de messages d'avertissement si on lançait Windows depuis un OS concurrent comme DR-DOS). Son usage d'Adobe Type

Manager rendait déjà la qualité d'affichage bien meilleure.

La version 3.1 poussa cette qualité un peu plus loin en remplaçant Adobe Type Manager par TrueType. Une version 3.11 (Windows for Workgroups) intégra

même de façon native l'usage du réseau local.

**Sommaire**

**Historique**

Ces versions avaient peu à peu fini par intégrer un noyau [réf. nécessaire], un shell propre similaire au DOS et des utilitaires de gestion du DOS, en plus de l'interface

graphique qui donna son nom au système d'exploitation. Elles furent donc considérées comme les successeurs de MS-DOS, avec le confort que chaque

périphérique comme chaque application n'avait besoin dès lors que du driver Windows, ce qui simplifiait considérablement la gestion de ceux-ci.

Avec Windows 95 (1995), l'OS (*Operating System*), épaulé par une importante campagne de publicité *grand public*, rencontre un grand succès, dû en partie au fait

que son éditeur a passé de très nombreux accords d'exclusivité avec les constructeurs d'ordinateurs leur interdisant d'installer un autre système sous peine de

sanctions financières . Il est vendu préinstallé sur la quasi-totalité des ordinateurs personnels.

Windows NT commencé en 1993, permet à Microsoft d'asseoir Windows dans les entreprises, suivi par Windows 2000.

Windows XP (mars 2002) marque un tournant : la fusion des systèmes grand public (Windows 98/Me) et professionnels (Windows NT, 2000). L'adoption de

celui-ci est progressive mais sa durée de vie en fera un système toujours répandu 10 ans après sa sortie. Il met aussi en évidence les lacunes de sécurité du noyau

utilisé et Microsoft est obligé de revoir intégralement le code de Windows en 2003.

Windows Vista se veut novateur mais souffre de gros défauts (lenteurs, stabilité) qui le rendent très impopulaire, malgré un Service Pack 1, passé inaperçu, qui en

résoudra la plupart.

Windows 7, qui est en quelque sorte une version entièrement achevée et optimisée de Windows Vista, connaît un grand taux d'adoption : son interface et sa

sécurisation figurent parmi ses atouts. Il est le système encore le plus plébiscité de tous les Windows .

Windows 8 est une nouvelle rupture pour Microsoft : plus ou moins un nouveau noyau, nouveau type d'applications téléchargeables depuis un Store, optimisation

de la consommation mémoire et processeur le rendant utilisable sur des configurations plus légères. Cependant, son interface trop pensée pour le tactile rebutera un

grand nombre d'utilisateurs, notamment par l'absence du « menu démarrer » historique qui les oblige à utiliser des logiciels tiers en remplacement comme Classic

Shell permettant de rajouter le menu démarrer de Windows 7 .

Windows 10 marque un nouveau tournant : code partagé avec les téléphones, interface revenant à un menu démarrer sur les PC traditionnels, promotion des

applications « universelles » (fonctionnant sur PC et smartphones à condition de posséder un matériel compatible). Cette version est aussi une grande nouveauté en

matière de gestion de mises à jour : elles sont permanentes et obligatoires (afin de renforcer la sécurité), et une nouvelle version de Windows doit sortir environ tous

les six mois. Un nouveau navigateur remplace Internet Explorer : Microsoft Edge. Le seul bémol étant l'excès de surveillance des utilisateurs par le biais de

programmes de diagnostics, de sauvegarde des historiques internet pour la prédiction de pages ou des données d'utilisation, qui rebutent encore beaucoup

d'utilisateurs. Cependant la majorité de ces options sont désactivables dans les paramètres de Windows.

Windows 11 remarque un tournant en lançant une nouvelle interface, un nouveau Microsoft Store, Microsoft Teams et application Xbox intégrées, en changeant

automatiquement le SDR en HDR (avec un écran compatible HDR), ainsi que l'arrivée des applications Win32 et Android sur le Store.

**Compléments**

Bill Gates a appelé son service d'exploitation Windows (fenêtres), car l'innovation principale du *shell* puis du système d'exploitation était l'emploi de fenêtres

d'affichage.

La gamme Windows est composée de plusieurs branches (voir la section *Branches techniques de Windows*) :

La première branche, dite *branche 16 bits*, couvre Windows 1 à 3.11 (3.2 en chinois). Elle est apparue en 1985 et fonctionnait uniquement

sur des PC compatibles, en mode 16 bits ;

La deuxième branche, dite *branche Windows NT* (Windows NT 3.1, NT 4.0, puis Windows 2000), est apparue en 1993. C’est un

développement repartant de zéro, destiné aux ordinateurs personnels, aux serveurs et à des ordinateurs non compatibles PC. Elle a

d’abord été utilisée dans les entreprises. Avec Windows XP, sorti en 2001, qui continue la branche Windows NT, cette branche est

désormais aussi grand public, et se poursuit avec Windows Vista, Windows 7, Windows 8 et Windows 10 ;

La troisième branche, parfois appelée *branche Windows 9x*, est apparue en 1995 et a existé parallèlement avec la branche NT. Cette

branche a débuté avec Windows 95, suivi de Windows 98 et Windows Me. Elle était plus connue du grand public et avait pour vocation de

remplacer la première branche. C’est la première branche grand public compatible 32 bits. L'interface graphique était compatible avec le

mode 32-bits mais basée sur l'OS MS-DOS (en version 7.1) nativement 16-bits avec néanmoins l'ajout de quelques modes de gestion

améliorée de la mémoire ;

La quatrième branche, dite *branche Windows CE*, apparue en 1996 avec Windows CE 1.0. Elle est destinée aux systèmes embarqués et

matériels légers et portables (assistant personnel, téléphone portable). C’est la base de Windows Mobile et Pocket PC ;

La cinquième branche, dite *branche Windows RT*, conçue exclusivement pour les processeurs ARM (architecture notamment présente

dans les tablettes).

**Branches techniques de Windows**

Les premières versions de Windows étaient lancées depuis DOS et utilisaient le système de fichiers de DOS.

Windows a immédiatement incorporé certaines fonctions de système d’exploitation, notamment un format

d’exécutable propre, la gestion des processus en multitâche coopératif (aux fonctionnalités peu probantes lorsque

des applications DOS y étaient utilisées), la gestion de mémoire virtuelle, et des pilotes pour gérer l’affichage,

l’impression, le clavier, le son, etc. Windows 2.10 pour 386 tirait également parti des nouvelles capacités de

l’Intel 80386, tel le placement du noyau en mode protégé et l’exécution des programmes DOS dans une machine

virtuelle en mode virtuel 8086.

On pouvait utiliser Windows avec d’autres DOS que le MS-DOS de Microsoft, comme PC-DOS d’IBM ou DRDOS,

sous réserve de passer outre les messages de dissuasion émis lors de l’installation. À partir de Windows 95,

l’interface graphique est devenue commercialement associée à MS-DOS. Cela a motivé un procès entre Caldera,

éditeur à l’époque de DR-DOS, qui permettait également de faire tourner Windows, et Microsoft. Caldera estimait

en effet que Microsoft adoptait ainsi une pratique anticoncurrentielle de *vente forcée*, sans fondement technique

réel.

À partir de Windows XP, on peut considérer que le DOS a bel et bien disparu des systèmes d’exploitation grand

public de Microsoft, bien qu’une émulation reste disponible.


**Branche Windows 9x**

Noms de codes connus entre parenthèses.

Windows 95 (*Chicago*) : 24 août 1995

Windows 95 OSR1 : 14 février 1996

Windows 95 OSR2 (*Detroit*) : 24 août 1996

Windows 95 OSR2.1 : 27 août 1997

Windows 95 OSR2.5 : 26 novembre 1997

Windows 98 (*Memphis*) : juin 1998

Windows 98 Seconde Édition (souvent abrégé en « Windows 98 SE ») : mai 1999

Windows Me (ou *Windows Millennium Edition*) : septembre 2000

À cause du noyau NT trop jeune et de l'utilisation importante de programmes tournant sous MS-DOS, Microsoft décida d’éditer un système d’exploitation à

destination du grand public, qui reprendrait certains avantages de Windows NT tout en restant compatible avec les versions antérieures de Windows et MS-DOS.

Les systèmes Windows 95 et suivants furent des évolutions hybrides 16/32 bits des versions Windows 3.0 et 3.1. Ils sont tous construits sur le même modèle de

pilotes : les VxD. En 1995, Windows 95 apporta plusieurs améliorations : le multitâche préemptif, la couche réseau inspirée de celle de NT, une interface

graphique nouvelle. Ce n’est pas un nouveau système d’exploitation, mais une évolution de Windows 3.1. Windows 95 devait pouvoir fonctionner sur des

configurations d’entrée de gamme avec 4 Mo de mémoire vive. La version OSR2 de Windows 95 apporta la prise en charge de l’USB et de FAT32.

Cette première mouture, connue durant son développement sous le nom de code « Chicago » et sortie sous le nom de Windows 95, a connu plusieurs évolutions,

dont Windows 98 et Windows Me (*Millennium Edition*), qui ont permis de confirmer la popularité des systèmes d’exploitation de Microsoft. Ses différentes

versions ont souffert d’une réputation [réf. nécessaire] d’instabilité et de vulnérabilité aux attaques par les réseaux. En 2001, Microsoft a décidé de mettre un terme à

cette branche en sortant Windows NT 5.1 connu sous le nom de Windows XP, plus stable et moins vulnérable [réf. nécessaire] (voir l'article Windows 9x).

**Branche Windows NT**

La branche NT (*Nouvelle Technologie*), est une famille de systèmes d’exploitation développés à partir de zéro, bien qu’elle soit une évolution de l’API de

Windows souvent appelée Win32. Windows NT est né du divorce de Microsoft et d’IBM sur le développement du système d’exploitation OS/2. Windows NT a

été développé pour concurrencer les systèmes utilisés en entreprise.

Le noyau serait inspiré de VAX VMS et d’Unix et apporte des concepts nouveaux, comme la notion d’objet permettant une utilisation uniforme. Il est conçu à

l’origine pour les processeurs de famille x86 (à partir de l’Intel 80386), MIPS, DEC Alpha et PowerPC. Il n’existait pour ces processeurs que des versions 32 bits,

bien que certains soient en 64 bits. Aujourd’hui, les familles x86, x86-64 et Itanium sont supportées, en 64 bits pour les deux dernières. L’arrêt successif du support

des différents processeurs est dû à des raisons économiques.

Elle permet le multitâche préemptif, le *multithreading*, un modèle d’exécution séparée (chaque processus possède une zone de mémoire séparée, sans accès à celle

des autres processus). Elle dispose aussi d'un système de gestion de fichiers propre, le NTFS (qui possède comme innovations principales les notions de droits

d'accès et de propriété) en plus de ceux utilisés par les branches antérieures, FAT12 (rapidement disparu à partir de Windows XP), FAT16 et pour Windows 9x,

FAT32.

Sa disponibilité pour le grand public a eu lieu avec la sortie de Windows XP, première version familiale à être fondée sur cette branche unifiée après le succès de

Windows 2000 dans sa version professionnelle.

**Branche Windows CE**

Cherchant à s’imposer sur le marché en pleine croissance des assistants personnels (PDA), Microsoft a développé une version légère de son système d’exploitation

et s’est associé aux grands constructeurs d’ordinateurs personnels pour pénétrer ce marché jusque-là dominé par Palm. Les produits exploitant ce type de plateforme

sont appelés Pocket PC. Adapté aux contraintes de ces machines (affichage, mémoire), Windows CE présente une interface similaire à celle de systèmes

d’exploitation pour PC bien que son noyau soit différent.

Avec la sophistication croissante des appareils ménagers, le but avoué de Microsoft est d’installer Windows CE (ou une version ultérieure) sur tous les appareils de

la maison, créant ainsi un univers domotique intégré. Des versions de Windows CE sont d’ores et déjà disponibles pour les téléphones portables. Depuis 2003,

l’appellation « Pocket PC » a été remplacée par « Windows Mobile ». Il existe donc Windows Mobile 2003 pour Pocket PC et Windows Mobile 2003 pour

smartphone.

Le binaire issu de la compilation d’un programme écrit en langage C# de Microsoft est automatiquement compatible avec cette plate-forme, si l’on suit certaines

restrictions (des bibliothèques liées en particulier). Il faut néanmoins vérifier la présence du Framework .NET sur la machine cible afin de pouvoir l’exécuter.

**Branche Windows RT**

Windows RT est une version du système d'exploitation Windows 8 pour les appareils ARM comme certaines tablettes. Le sigle RT n'a pas de signification

officielle. Contrairement à d'autres systèmes d'exploitation Windows, il ne pourra qu'exécuter les applications ayant été certifiées par Microsoft et placées dans le

magasin de Windows Store.

Windows RT inclut d'autres applications telles que Microsoft Office Word, Excel, PowerPoint et OneNote 2013 RT. Microsoft vend uniquement ce système

d'exploitation aux fabricants d'appareils directement, et non comme un produit autonome pour les consommateurs. Par sa compatibilité logicielle qu'avec les

logiciels du Windows Store, cette version de Windows fut arrêté avec Windows 10. Néanmoins, en 2017 Microsoft sort Windows 10 S, système d'exploitation très

proche de Windows RT mais destiné au marché de l'éducation .

**Construction**

Un système d'exploitation tel que ceux de la série Windows est un ensemble de programmes qui manipule les moyens matériels de l'ordinateur et offre aux logiciels

applicatifs des services en rapport avec leur utilisation . Les principaux programmes internes de la série Windows NT sont la couche d'abstraction matérielle, le

noyau, les pilotes, le système graphique, et les interfaces de programmation Win32 et POSIX. Une partie des programmes sont exécutés en mode noyau, et le reste

est exécuté en mode utilisateur. Lorsqu'un programme est exécuté en mode utilisateur, ses possibilités sont limitées: l'accès à certains emplacement de mémoire et

l'exécution de certaines instructions est interdite. les produits de la famille Windows comportent différents programmes permettant l'exécution d'applications dans

différents environnements ayant chacun sa propre interface de programmation: MS-DOS, OS/2, POSIX ou Win32 - ce dernier, peu utilisé au début, est devenu

l'environnement de référence, utilisé par la majorité des applications pour Windows .

L'interface de programmation Win32 est un très large ensemble de fonctions, utilisées par les applications notamment pour manipuler des processus, des threads,

utiliser les réseaux informatiques, les périphériques, afficher des fenêtres, des dialogues, des widgets, ou traiter des erreurs. Il permet également d'utiliser des

services tels que DirectX, GDI, OpenGL ou MAPI, ainsi que des services offerts par des objets COM ou ActiveX. Win32 est disponible sur les produits des

branches Windows 9x, Windows NT et Windows CE .

Les produits de la famille Windows sont livrés avec une gamme de programmes qui permettent de leur ajouter des fonctionnalités, parmi lesquels des jeux, des

serveurs et des applications .

![](assets/Aspose.Words.81436e47-4625-4d0a-973e-e05f97b3faeb.001.png)

**Systèmes abandonnés par Microsoft**

Microsoft a développé d’autres systèmes que ceux que l’on connaît, cependant ces derniers ont été abandonnés pour des raisons diverses :

Microsoft Neptune conçu à la base pour donner une version familiale de Windows 2000, est en quelque sorte le projet embryonnaire de

Windows XP ;

Microsoft Bob conçu pour remplacer le gestionnaire de programme dans Windows 3.1x et Windows 95 mais a été un échec flagrant qui a

conduit à son abandon rapide (avant la sortie de Windows 98).

**Systèmes non distribués par Microsoft**

Certains autres systèmes assurent une compatibilité plus ou moins complète avec Windows :

ReactOS est un système d’exploitation en développement visant à être compatible avec NT 5 ;

Wine est un ensemble de bibliothèques permettant d’exécuter des logiciels conçus pour Windows sur les systèmes Unix et Linux.

![](assets/Aspose.Words.81436e47-4625-4d0a-973e-e05f97b3faeb.002.png)


**Un système d’exploitation controversé**

Au cours des années 1990, en particulier avec la version 95, Windows couvre les neuf dixièmes du marché des systèmes d’exploitation et des applications

bureautiques pour PC. En janvier 2007, il était installé sur plus de 95 % des ordinateurs personnels , la grande majorité des ordinateurs étant vendus avec un

système Windows préinstallé par le constructeur (licence OEM). En conséquence, ses concurrents l’ont accusé de monopole et de pratiques commerciales

déloyales et ont engagé des poursuites *antitrust* à son encontre dans de nombreux pays, notamment aux États-Unis et en Europe.



**Chronologie des sorties**

**Un système d’exploitation controversé**



17Windows est aussi un produit techniquement très critiqué par certains. Il est notamment reproché à ce système son instabilité (par exemple les écrans bleus de la

mort) et sa vulnérabilité. Pourtant, sa popularité n’a jamais été menacée, principalement parce que les clients particuliers ne peuvent pas refuser l’achat de ces

logiciels en vente liée. Cet avantage concurrentiel a amené Microsoft à détenir une incontestable suprématie en matière de culture informatique, le grand public

n’imaginant même pas qu’un ordinateur personnel puisse fonctionner avec des systèmes d'exploitation autres que Windows, alors même qu’il existe des

alternatives - commerciales ou non - pouvant répondre à ses besoins, comme Mac OS (incompatible avec les PC), ainsi que les nombreuses distributions Linux et

les systèmes \*BSD ou AmigaOS (incompatibles avec les PC).

Ces pratiques de vente subordonnée des systèmes d’exploitation Microsoft lors de l’achat d’ordinateurs neufs font que des utilisateurs considèrent ces logiciels

Windows, quels que soient leurs défauts et qualités, comme des « racketiciels » en référence à l'illégalité de la pratique, au moins en France.

En janvier 2004, le Commissaire européen à la concurrence, Mario Monti, a ordonné à Microsoft de remédier à la fourniture systématique de son lecteur

multimédia *Windows Media Player* dans Windows XP. Se pliant à cette exigence, Microsoft a tenté de mettre en vente *Windows Limited Media Edition* au même

prix que la version normale avec *Windows Media Player*. Ce procédé a déplu à la Commission, laquelle demandait à la fois que l’entreprise fournisse ce produit

pour « améliorer la situation du marché », tout en ne le faisant pas savoir. Se pliant à cette dernière exigence, Microsoft a donc offert, à partir du 14 juin 2004, une

version de Windows amputée à grands frais de ce logiciel sous la dénomination de Windows XP « N », mais qui n’a pas été un succès commercial. Il s’avère que,

non seulement Real Player - qui avait inspiré ces poursuites pour concurrence déloyale - reste compétitif, mais qu’il utilise même des ressources de *Windows Media*

*Player* pour son fonctionnement.

Certains considèrent que la position dominante de Microsoft sur le marché des particuliers est due à sa volonté présumée de rendre ses systèmes d’exploitation

incompatibles avec les autres, notamment en ne respectant que peu ou pas les normes permettant l’interopérabilité entre programmes, et au fait qu’il est le plus

souvent fourni avec les ordinateurs à l’achat. Le quasi-monopole de Microsoft dans le domaine des systèmes d’exploitation pour PC encourage les développeurs

d’applications à offrir leurs logiciels pour la seule plate-forme Windows, ce qui est pour eux le plus sûr moyen de toucher de nombreux clients, tout en leur

permettant en outre d'importantes économies dans leur développement et leur commercialisation. Microsoft leur rend donc le service de standardiser le marché

mieux que ne pourrait le faire une autorité réglementaire, toujours moins bien informée. L’avantage est réciproque, puisque cela rend Microsoft Windows plus

intéressant pour leurs clients. Une autre caractéristique des systèmes d’exploitation est que les programmes qui fonctionnent avec l’un ne fonctionnent pas

nécessairement avec l’autre. Ainsi, alors que Microsoft fait en sorte que les anciens programmes fonctionnent avec ses nouveaux systèmes Windows, une personne

désirant quitter Windows doit obligatoirement racheter ses logiciels. En outre, il est difficile pour un développeur de porter un programme écrit pour Windows vers

un autre système d’exploitation.

Pour remédier à ces problèmes, il existe des bibliothèques facilitant la portabilité (OpenGL, OpenAL, GTK+, Qt, SFML, SDL…). D’autres bibliothèques

permettent de faire fonctionner des applications Windows (Win32) sur des systèmes de type Unix (comme Linux ou \*BSD), par exemple Wine, l’utilisation de ces

bibliothèques se faisant de manière transparente pour l’utilisateur.

À l'inverse, des logiciels implémentés sous d'autres systèmes d'exploitation comme GIMP se sont vus portés sous Windows, ainsi que beaucoup d'autres logiciels

GNU existant sous Linux. Le portage des logiciels Linux sous Windows est facilité par des environnements comme MinGW, Cygwin, Qt, qui permettent aux

programmes Open source Linux d'être reconstruits sous Windows à partir de leur code source : ainsi ce portage ne requiert généralement que peu de modifications.

Entre 2004 et 2013, Microsoft est condamné à plus de 2 milliards d'euros d'amende par la Commission européenne pour

entrave à la concurrence et divers abus de position dominante , notamment en raison de l'utilisation de sa position

sur le marché des systèmes d'exploitation pour imposer Internet Explorer (qui deviendra un monopole de fait pendant une

décennie), ainsi que pour ne pas avoir mis en place le « Ballot Screen » qui lui avait été imposé par l'Union Européenne,

et qui permettait aux utilisateurs de choisir un autre navigateur (un problème technique selon Microsoft).

Microsoft est critiqué depuis des années pour ses pratiques anticoncurrentielles dans le monde. Le département de la

Justice des États-Unis a révélé que Microsoft utilise le slogan «*Embrace, extend and extinguish* » ou « ***Embrace,***

***extend, and exterminate*** » (en français, « Adopte, étend et anéantit/extermine ») pour décrire sa stratégie d’introduction

de produits : adopter des standards largement utilisés, les étendre de façon à créer des standards propriétaires, puis utiliser
