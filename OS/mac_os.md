# Mac OS

### Introduction

Mac OS est un système d'exploitation développé par Apple, sa première version est sortie le 24 janvier 1984. 
Son système est vendu avec le matériel, contrairement au système Windows.

### Historique des principales versions 

##### System 0.0

Le premier système d’exploitation d’Apple est né en 1984. Il ne porte pas encore le nom de « Mac OS » mais tout simplement « system 0.0 »
Entre 1984 et 1988 le « system » n’évoluera pas beaucoup et souffrira de bugs et d’une stabilité perfectible.

![System 0.0](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/macossystem1.jpg)

##### Mac OS 8 

C’est en 1997 qu’apparaît la dénomination de « Mac OS » (Macintosh Operating System). C’est la huitième version du système d’exploitation qui hérite de ce nom.
Cette version inaugure une interface graphique entièrement nouvelle (Platinium) C’est aussi cette version qui embarquera les applications pour l’arrivée d’internet.

![Mac OS 8](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/macos8-600x450.png)

##### Mac OS 9

Cette version est très proche de la précédente. Elle sert de transition avant l’arrivée de Mac OS X. Elle est très aboutie et très stable et est entièrement compatible Power PC. L’interface s’est tout de même affinée.

![Mac OS 9](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/macos9-600x450.jpg)

##### Mac OS X 10.0 « Cheetah »

C’est la première version de Mac OS X qui sort en 2001. Cette version fait apparaître le Dock et un style graphique (Aqua) fait de transparence et d’ombres qui suivront les versions ultérieures.

![Mac OS X 10.0 « Cheetah »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.0cheetah-600x450.jpg)

##### Mac OS X 10.1 « Puma »

Il s’agit de la première grosse mise à jour de Mac OS X. Elle sort en septembre 2001. La grosse innovation réside dans sa vitesse d’exécution bien plus rapide que les versions précédentes.

![Mac OS X 10.1 « Puma »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.1puma-600x450.jpg)

Il s’agit de la première grosse mise à jour de Mac OS X. Elle sort en septembre 2001. La grosse innovation réside dans sa vitesse d’exécution bien plus rapide que les versions précédentes.

##### Mac OS X 10.2 « Jaguar »

Cette version inclut quelques nouveautés comme ichat. Graphiquement le dock perd ses rayures pour un fond uni.

![Mac OS X 10.2 « Jaguar »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.2jaguar-600x450.jpg)

##### Mac OS X 10.3 « Panther »

Panther est encore plus rapide que son prédécesseur et adopte un style aluminium brossé pour ses fenêtres. Une colonne à gauche apparaît pour faciliter la navigation. Le changement d’utilisateur est également facilité avec un effet de cube 3D qui tourne (la classe J).

C’est également l’arrivée « d’exposé » qui permet de visualiser toutes les fenêtres ouvertes d’un seul coup d’œil.

![Mac OS X 10.3 « Panther »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.3panther-600x450.png)

##### Mac OS X 10.4 « Tiger »

Tiger est toujours plus rapide et le système de recherche Spootlight fait son entrée ainsi que le dashboard (une nouvelle fonction d’exposé)

![Mac OS X 10.4 « Tiger »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.4tiger-600x450.jpg)

##### Mac OS X 10.5 « Léopard »

C’est une mise à jour majeure de l’OS. L’interface graphique évolue grandement avec l’apparition des piles, d’un dock en 3D, les icônes sont entièrement revues, la présentation des fichiers sous de cover flow apparaît également.

C’est également l’arrivée de Time machine qui permet de restaurer un fichier/dossier ou le système entier de la façon dont il était quelques minutes auparavant. Une révolution !

![Mac OS X 10.5 « Léopard »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.5leopard-600x375.jpg)

##### Mac OS X 10.6 « Snow Leopard »

Cette version abandonne les Power PC au profit des Intel, elle est plus stable et plus sécurisée. Les évolutions sont surtout dans le code informatique.

![Mac OS X 10.6 « Snow Leopard »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.6snow-leopard-600x375.png)

##### Mac OS X 10.7 « Lion »

Pour la première fois, cet OS n’est pas vendu avec un support physique. Il faut le télécharger via l’Appstore. Ce dernier fait d’ailleurs partie des nouveautés. Le launchpad fait également son apparition. L’interface évolue également.

![Mac OS X 10.7 « Lion »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.7lion-600x338.jpg)

##### Mac OS X 10.8 « Mountain Lion »

Parmi les évolutions, on peut noter une meilleure intégration d’icloud, l’application Messages remplace Ichat, intégration des réseaux sociaux ou encore un centre de notification.

![Mac OS X 10.8 « Mountain Lion »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.8mountain-lion-600x380.jpg)

##### Mac OS X 10.9 « Maverick »

C’est la première qui abandonne les appellations de félin et surtout qui est gratuite ! Une première pour Apple
Les applications ibook et Plans (déjà disponibles sur iOS) font leur apparition, l’interface de Safari est renouvelée

![Mac OS X 10.9 « Maverick »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.9maverick-600x375.jpg)

##### Mac OS X 10.10 « Yosemite »

Dans cette version l’interface graphique évolue significativement pour suivre la tendance du flat design. Les fenêtres sont plus claires, un mode nuit apparaît et on note l’ajout de flou et de transparence.
Spotlight, Safari font peau neuve. Il est possible d’envoyer des SMS/Appels depuis le Mac grâce à un iPhone synchronisé.

![Mac OS X 10.10 « Yosemite »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.10yosemite-600x338.jpg)

##### Mac OS X 10.11 « El Capitan »

Cette nouvelle version se concentre essentiellement sur l’ergonomie et les performances. On note parmi les nouveautés :

*Split view pour travailler sur 2 fenêtres d’application en même temps. *Le curseur d’attente a été revu *Safari peut muter le son d’un onglet

![Mac OS X 10.11 « El Capitan »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.11elcapitan-600x375.jpg)

##### Mac OS X 10.12 « Sierra »

Sierra inaugure un nouveau système de fichiers 64bits. Ce dernier est plus adapté aux SSD. On note également l’arrivée de Siri et d’Apple Pay.

![Mac OS X 10.12 « Sierra »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.12sierra-600x376.jpg)

##### Mac OS X 10.13 « High Sierra »

![Mac OS X 10.13 « High Sierra »](https://www.ouestpc.fr/wp-content/uploads/sites/795/2018/06/10.13high-sierra-600x338.jpg)

### Caractéristiques 

MacOS comprend tous les services communs et coquille UNIX, une commande complète machine Java mis en œuvre en mode natif[8], ainsi que les principales langues scripts comment Perl, PHP, tcl, rubis et python et il est certifié par The Open Group comme conforme à la norme UNIX 03[9][10], depuis sa sortie 10.5 partir.

Mac OS a été créé en combinant:

Darwin, un système d'exploitation libre et complet développé par Apple Inc., la famille open Source BSD dérivé de Unix, avec noyau XNU basé sur micronoyau Mach, inspiré et hybridée avec le noyau du système d'exploitation FreeBSD avec des portions de code NetBSD[11];
Une série de kit d'E / S, les cadres, un ensemble de bibliothèques optimisées qui facilitent le code UNIX portage et quelques ensembles de API (Charbon et de cacao), développé par NeXTstep et Apple Inc;.
Une interface graphique (GUI) appelé aqua, développé par Apple Inc;.
Le système d'exploitation a obtenu sa première version commerciale 2001.

Il est mis en œuvre pour les processeurs PowerPC G3, PowerPC G4 et PowerPC G5, produits IBM et Motorola (maintenant Freescale Semiconductor), Pour les processeurs Intel (Probablement sur tous les processeurs avec des instructions SSE2) Et pour les processeurs ARM Cortex.

MacOS conservé temporairement, jusqu'à ce que la sortie de la version Leopard PowerPC, une compatibilité descendante presque avec des applications Macintosh également très daté, grâce à la caractéristique de pouvoir charger, le cas échéant, Mac OS classique dans un machine virtuelle l'intérieur d'une spéciale tâche fermé et isolé du reste du système. En outre, l'API carbone Ils ont permis une conversion rapide de nombreuses applications de Mac OS 9 à Mac OS X, donnant ainsi à compléter la transition.

Une technologie similaire, appelé Rosetta, Il est utilisé pour permettre l'utilisation des applications compilées pour les processeurs PowerPC sur les machines Intel (il est installé dans un automatique et sans frais si et lorsque cela est nécessaire). L'héritage du NeXTSTEP est toujours présent, il est aussi à noter que beaucoup commencent primitive avec les lettres « NS » (contraction de NeXTStep). L'API NeXTstep / OpenStep ont été intégrés dans le système d'exploitation de l'API cacao.

MacOS est en mesure d'exécuter directement de nombreux programmes BSD et GNU / Linux par la compilation de sources (MAKE) et, dans certains cas, grâce à l'utilisation de la gestion graphique via apple X11, Cette série dans le système d'exploitation, même si normalement utilisé par les applications pour Mac OS en utilisant nativement que le moteur graphique quartz.

Il y a plusieurs projets qui permettront de vérifier les nombreux disponibles binaires précompilés (forfaits): à savoir, empêche l'utilisateur d'avoir à configurer et compiler les fichiers sources (ports), une opération est pas toujours facile, car ils sont généralement de la ligne de commande est exécutée (Terminal). Les plus célèbres sont le projet mouchard et le projet DarwinPorts.

La version 10.3 est le premier à inclure apple X11, La version d'Apple du gestionnaire graphique X11 applications Unix, fournis à titre de composant d'installation standard en option. X11 d'Apple est basé sur le projet XFree86 version 4.3 et X11R6.6, et fournit un gestionnaire graphique intégré avec MacOS, car il partage le même aspect des fenêtres. Utilisez la technologie Quartz pour les graphiques et le design d'interface profite de l'accélération OpenGL. Des exemples d'applications pour Mac OS à l'aide de serveurs X11 sont: OpenOffice.org et boiteux, à la fois pour Mac. Le X11 a été