Depuis la venue d’Android Studio, cet environnement de travail s’impose et séduit les développeurs Android par ses fonctionnalités, sa rapidité et son socle logiciel solide. Depuis sa version 1, sortie le 8 décembre 2014, Google le présente comme l’IDE (pour Integrated Development Environment) officiel dans le développement Android. Ni les débutants, ni les professionnels peuvent faire l’impasse sur son utilisation.

Les explications données sont, au plus possible, accessibles à toute personne peu importe son niveau. Par contre, ce chapitre n’aborde pas toutes les manipulations et les fonctionnalités de ce logiciel tant elles sont nombreuses. Le socle logiciel d’Android Studio est l’IDE IntelliJ IDEA et il bénéficie alors de toutes ses fonctionnalités. Seulement les fonctionnalités les plus importantes et notables en dehors de celles d’IntelliJ IDEA sont abordées. De ce fait, disposer d’une expérience avec IntelliJ IDEA vous aidera dans la prise en main d’Android Studio mais ne vous pénalisera pas dans le cas contraire.

Ce chapitre se sépare en différentes sections. La section 1 explique la création des différents projets Android. Rappelez-vous que Android ne se limite plus aux téléphones et aux tablettes. Aujourd’hui, des voitures, des télévisions ou même des montres tournent sur ce système. La section 2 touche quelques mots sur le moteur de production Gradle mais les explications sont modestes au vu des innombrables fonctionnalités de cette technologie. Pour finir, la section 3 aborde le confort d’utilisation du logiciel tant au niveau des ressources XML que dans le code source Java.

Le développement d’Android Studio est encore très actif. Prenez donc note que toutes les explicitations données dans les sections à venir ont été rédigés avec sa version 1.3. Certaines des manipulations expliquées peuvent différer si vous utilisez une version plus récente qui serait sortie après la rédaction de ce chapitre. Si vous détectez des différences dans les captures d’écran ou dans les manipulations exposées, n’hésitez pas à me contacter par messagerie privée.

Créer un projet Android
Le moteur de production Gradle
Confort d'utilisation
Créer un projet Android

Si vous installez Android Studio pour la première fois, vous passerez par quelques écrans d’installation pour le configurer. Durant ces étapes préliminaires, pensez simplement à spécifier la localisation de votre SDK Android si vous en disposez déjà d’un sur votre poste de travail. Il serait dommage d’en installer un autre à un endroit différent au vu de la place qu’il peut prendre (peut dépasser les 20 Gigas).

Par contre, si c’est une première expérience pour vous le développement Android, choisissez l’installation standard dans cet enchainement d’écran pour installer le SDK Android au passage. Android Studio vous installera le logiciel et le SDK avec le support de la dernière version Android.

Pour rappel, ce logiciel est disponible sur cette page de la documentation officielle et disponible sur toutes les plateformes courantes, à savoir Windows, Linux et OS X.

Message de bienvenue de la part d'Android Studio
Message de bienvenue de la part d’Android Studio
L’installation terminée, vous pouvez créer votre premier projet sous Android Studio. Lancez la procédure de création d’un projet pour démarrer un nouvel enchainement d’écran où nous spécifierons toutes les informations nécessaires au projet et les plateformes Android qu’il supportera.

Configurez votre nouveau projet Android avec son nom et sa localisation
Configurez votre nouveau projet Android avec son nom et sa localisation
Rappelez-vous que toutes les sources exposées dans ce tutoriel sont disponibles sur ce projet GitHub. Il n’y a aucune obligation de votre part de suivre à la lettre les manipulations données mais puisque le projet LearnIt sert de référence aussi bien les sources que les informations sur le projet sont en accord avec cette référence.

Donc, sur notre premier écran, pour la configuration d’un nouveau projet, nous renseignons "LearnIt" comme nom d’application. Ce nom sera utilisé pour le projet sur votre ordinateur et Android Studio va créer une nouvelle ressource qu’il liera avec le nom de l’application pour l’afficher sur les terminaux Android (réel ou non).

Si vous êtes un développeur Android, vous connaissez l’importance du nom de domaine de la compagnie. Pour les débutants, voici les explications. En fait, plusieurs applications peuvent avoir le même nom. Il peut alors y avoir un problème de différenciation entre une application LearnIt provenant d’un développeur A et une autre application LearnIt d’un développeur B. Google demande alors de spécifier un nom de domaine unique pour pouvoir identifier ces 2 applications.

Dans cet exemple, le nom de domaine spécifié est "randoomz.org". Il est vivement conseillé de ne pas l’utiliser pour vous si vous comptez publier votre application Material Design sur le Play Store Android. Ce nom de domaine est déjà utilisé et vous serez bloqué à l’étape de publication. Cela dit, ce nom de domaine n’est pas obligé d’être un nom de domaine réel. Soit vous avez déjà un nom de domaine et dans ce cas, utilisez le ici. Sinon, inventez en un nouveau que vous utiliserez pour toutes vos applications. Si à l’étape de publication, le nom de domaine est utilisé, pas de panique, il sera encore temps de le modifier.

Pour finir, spécifier la localisation du projet. Par convention, créez votre projet dans un dossier nommé "workspace". C’est une convention un peu vieille mais qui permet de ne pas mélanger vos documents avec vos projets. Puis, spécifiez à nouveau le nom du projet pour que Android Studio crée tous les fichiers nécessaires dans ce même répertoire.

Sélection des plateformes Android sur lesquelles l'application pourra s'exécuter
Sélection des plateformes Android sur lesquelles l’application pourra s’exécuter
Spécifions maintenant les plateformes Android avec lequel l’application sera compatible. Malheureusement, ce tutoriel n’aborde pas le développement sur les montres, les télévisions, les voitures, ni les Google Glass (oui, ce produit existe toujours !). Contentez-vous de cocher "Phone and Tablet". Le reste des plateformes viendront avec d’autres tutoriels. En attendant, la documentation aborde le développement sur chacune de ses plateformes, il suffit de choisir son guide.

Sélection d'une Activity par défaut pour commencer le projet
Sélection d’une Activity par défaut pour commencer le projet
Depuis 2009, Google a bien compris que le développement Android pouvait être répétitif sur certains aspects. Globalement, à chaque création de projet, il était courant de développer un écran précédemment développé dans un autre projet. Par exemple, il serait légitime de développer un premier écran d’identification pour des applications mobiles de Zeste de Savoir, GitHub ou tous les autres sites avec un processus d’identification. Dans ce cas, Android Studio propose un canevas nommé "Login Activity" où il génèrera toutes les classes et les ressources nécessaires à la création de cet écran. C’est une fonctionnalité pratique et appréciée des développeurs.

Pour cet exemple, nous créons un écran simple grâce à l’option "Blank Activity". Certes, c’est moins fun mais pour des questions d’apprentissages, c’est mieux de commencer de zéro plutôt que d’avoir une tonne de truc générés dès le départ que vous ne comprendrez pas. Sachez quand même qu’il sera possible de générer tous ces canvas à tout moment durant le développement de votre application. Donc si vous ne faites pas joujou avec eux maintenant, il sera possible de le faire plus tard. La procédure sera expliquée dans une prochaine section de ce chapitre.

Personnalisation des informations sur l'Activity choisie précédemment
Personnalisation des informations sur l’Activity choisie précédemment
Dernière étape, la personnalisation de l’écran choisi. Dans notre cas, la personnalisation est simple puisqu’il n’y a qu’une Activity et ses ressources. Si vous avez opté pour une autre Activity, aucune inquiétude. Les valeurs par défaut remplies par Android Studio suivent des standards qu’il est bon de suivre. D’ailleurs, nous ne modifierons rien sur cet écran dans le cadre de ce tutoriel.

"Activity Name" est le nom de la classe qui étendra Activity. Par convention, elle a l’habitude de se nommer MainActivity pour la qualifier d’écran principal.
"Layout Name" est le nom de la vue associée à l’écran sous la forme d’un fichier XML. La convention voudrait de convertir le nom de l’écran associé avec des underscore en renseignant le type du contrôleur avant (Activity, Fragment ou View).
"Title" est le nom de l’écran. Une ressource sera créée et sera utilisée dans l’Android Manifest pour spécifier à Android que le nom de cet écran sera celui indiqué dans la ressource créée.
"Menu resource name" est le nom de la ressource du menu de cet écran. Même si vous n’avez pas besoin d’un menu, Android Studio vous en créera un. Si vous voulez le retirer, vous devrez le faire à posteriori en supprimer le fichier XML et en retirer le code Java associé au menu dans l'Activity créé. Rien de bien compliqué et on notera la possibilité de pouvoir indiquer à Android Studio de ne rien générer du tout.
Le moteur de production Gradle

Cette section est difficile à expliquer. Gradle est un outil ultra puisant qui vous permettra de faire des tonnes de choses. Contrairement à Maven, qui est souvent perçu comme son prédécesseur bien que loin d’être enterré, Gradle n’est pas qu’un simple gestionnaire de dépendances mais un moteur entier de production. Grâce au Groovy, langage utilisé dans les scripts Gradle, vous pourrez exécuter des tâches complexes qui ne sont pas toujours liées directement à votre projet comme effectuer des commandes Git ou consolider un certain nombre de ressources externes pour votre projet.

Vous conviendrez qu’il serait utopique de vous expliquer dans son intégralité même les bases de Gradle. D’ailleurs, sa documentation officielle est juste gigantesque, de quoi décourager pas mal de développeurs, et aborde peu, voire pas du tout, les instructions spécifiques à Android alors que c’est cette même plateforme qui a contribué fortement à son utilisation au delà des simples projets Android. Aujourd’hui, beaucoup de projets Java, Groovy, C, C++, etc. utilisent Gradle.

Alors pourquoi Android utilise Gradle ? Et qu’est ce que nous allons aborder dans cette section ? Pour répondre à la première question, de mon propre ressenti, Google devait se sentir à l’étroit dans Maven. Pour avoir connu l’avant Gradle, il était souvent pénible de gérer ses dépendances, de mettre en production une application ou même d’insérer une dépendance Android dans Maven pour avoir un environnement de travail confortable. Bien que complexe, Gradle reste une alternative sérieuse et s’intègre bien mieux dans l’eco-système. Pour répondre à la seconde question, nous aborderons uniquement ce qui est spécifique à Android tout en expliquant certaines notions de base pour savoir ce que vous faites. Inutile de copier/coller bêtement ce qui est écrit dans ce tutoriel. Sinon, il suffit de se rendre sur les sources de ce tutoriel et d’en copier le contenu dans votre projet en modifiant ce qui vous plait.

Hiérarchie d’un projet
Avant de parler de Gradle, parlons de la hiérarchie d’un projet Android avec Gradle, de ses différents répertoires et fichiers. Le projet standard est souvent multi-module, c’est-à-dire que sa racine ne contient pas de sources mais des modules qui contiennent ces sources. Dans le cadre des sources de ce tutoriel, un seul module est créé et, est nommé "app" par convention. Ce module contient toutes les sources de l’application Android et toutes ses ressources.

A quoi pourrait correspondre d’autres modules ? Par exemple, si vous avez décidé de créer une version wearable (adapté pour les montres) de votre application, un nouveau module est nécessaire pour contenir toutes les sources de cette application. Puis, si cette application partage du code commun avec l’application téléphone, un troisième module aurait été une bonne solution pour contenir toutes ces sources et figurer comme dépendance au module wearable et du téléphone. Un module peut donc être une application à part entière ou une bibliothèque.

Toujours à la racine du projet, une série de répertoires et de fichiers sont exclusivement pour Gradle. Ce sont tous des fichiers standards qui sont utilisés dans n’importe quel projet Gradle, Android ou non. Il y a le répertoire gradle et les fichiers gradle.properties, gradlew et gradlew.bat. Le "w" à la fin des fichiers signifiant wrapper . En fait, ils aident à l’utilisation de Gradle au sein de votre environnement de développement et installent un environnement commun sur tous les intervenants d’un projet. C’est ainsi qu’un projet Gradle rendu open-source à travers une plateforme comme GitHub rend la prise en main et la collaboration plus aisée. Dans le cadre de ce tutoriel, vous verrez qu’on utilisera ce wrapper en ligne de commande. Cela sera expliqué par la suite.

Pour finir, nous avons les fichiers gradle. Il y a build.gradle et settings.gradle a la racine du projet et un build.gradle à la racine de chaque module. Dans le fichier settings.gradle, vous y trouvez la spécification de tous les modules du projet. Vous devriez voir simplement la ligne include ':app' puisque nous n’avons qu’un seul module. Si nous en créons un nouveau avec comme nom "lib", nous aurons alors include ':app', ':lib'. Vous pouvez créer des modules à la main et modifier ce fichier mais sachez qu’Android Studio permet aisément la création d’un module dans un projet. Pour ce faire, utilisez le menu File > New > New Module. Après avoir renseigné les informations au sujet du module, Android Studio se chargera du reste.

Quant aux fichiers build.gradle, ils seront expliqués juste après. En fait, ils sont plus ou moins liés (du moins, du fils vers le parent) et définissent toute la configuration de construction du projet. Ce sont dans ces fichiers que vous spécifiez les dépendances, les plugins et la configuration Android de vos modules.

Configuration top-level Android
Les fichiers build.gradle spécifient toutes les informations concernant la configuration Android et peuvent vite devenir complexes. Par exemple, nous aurions pu parler des variantes, grâce aux flavors, une fonctionnalité typique du plugin Android et qui manque terriblement aux projets Java standard. Malheureusement, ce concept est un peu trop avancé pour ce tutoriel mais pourrait faire l’objet d’un autre tutoriel. Dans la plupart des cas, vous n’aurez pas besoin d’éditer le fichier build à la racine du projet, seulement ceux des modules. Mais il est utile de savoir comment ils fonctionnent pour mieux pouvoir éditer ceux des modules. Celui de LearnIt ressemble à ceci :

// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
  repositories {
    jcenter()
  }
  dependencies {
    classpath 'com.android.tools.build:gradle:1.2.3'
    classpath 'com.neenbedankt.gradle.plugins:android-apt:1.4'

    // NOTE: Do not place your application dependencies here; they belong
    // in the individual module build.gradle files
  }
}

allprojects {
  repositories {
    jcenter()
  }
}
Configuration racine du projet Gradle Android
Qu’avons-nous là ? Dans allprojects { ... }, nous définissons tout ce qui est en commun à tous les modules de votre projet. Vous remarquez que les modules sont tellement souvent des applications que l’instruction est nommé "Tous les projets" et non pas "Tous les modules". La seule chose que nous spécifions commune à tous les modules est le dépôt où Gradle va chercher les dépendances que nous spécifions.

Alors pour ceux qui ne connaissent pas les gestionnaires de dépendances comme Maven, voici une brève explication pour savoir comment cela fonctionne : il existe sur internet de nombreux dépôts publics ou privés qui hébergent les binaires de bibliothèques. Tout le monde peut créer de nouveaux dépôts ou utiliser les dépôts existants. Cependant, il y a des dépôts plus utilisés que d’autres. Par exemple, Maven Central est sans doute l’un des plus gros dépôts Maven. Il regroupe des milliers de binaire qui sont en accès libre et tout le monde peut y placer ses propres binaires (sous demande). Dans vos projets Maven ou Gradle, il suffit alors de spécifier une dépendance vers l’un de ces binaires pour les télécharger sur votre machine de développement et l’utiliser. Les outils comme Maven et Gradle s’occupent eux-même de rajouter ces dépendances dans le classpath de votre projet.

Note : Le classpath est un paramètre passé à la JVM qui définit le chemin d’accès au repertoire où se trouvent les classes Java et les binaire afin qu’elle les exécute.

Revenons à la configuration commune à tous les modules, son contenu doit être plus clair. Nous spécifions simplement à Gradle qu’il doit chercher les dépendances dans le dépôt jcenter, une alternative à Maven Central.

Dans buildscript { ... }, nous configurons les classpaths nécessaires aux scripts Gradle des sous modules. Premièrement, nous spécifions le dépôt Maven des dépendances buildscript. Puis, nous spécifions les classpaths voulues.

classpath 'com.android.tools.build:gradle:1.2.3' est un classpath essentiel. C’est le plugin Android Gradle qui permet la configuration des modules du projet. Il est généré automatiquement à la création du projet.
classpath 'com.neenbedankt.gradle.plugins:android-apt:1.4' a été rajouté pour un autre plugin. Il sera expliqué plus tard mais sachez que vous en avez pas forcément besoin. D’ailleurs, il est plus probable que vous n’en aurez jamais besoin.
Configuration des modules
Dans la section précédente, nous avons configuré tout ce qui était commun aux modules. Tous les plugins utilisés et les dépendances renseignées sont disponibles grâce à la configuration de ce fichier build à la racine du projet. Sauf si vous désirez renseigner des dépendances spécifiques à un module précis, vous ne renseignerez pas de dépôt dans les fichiers build des modules. Voici un exemple tiré du projet de ce tutoriel :

apply plugin: 'com.android.application'
apply plugin: 'com.neenbedankt.android-apt'

android {
  compileSdkVersion 22
  buildToolsVersion "22.0.1"

  defaultConfig {
    applicationId "org.randoomz.learnit"
    minSdkVersion 15
    targetSdkVersion 22
    versionCode 1
    versionName "1.0"
  }
  buildTypes {
    release {
      minifyEnabled false
      proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
    }
  }
}

dependencies {
  compile 'com.android.support:support-v4:22.2.0'
  compile 'com.android.support:appcompat-v7:22.2.0'
  compile 'com.android.support:design:22.2.0'
  compile 'com.android.support:recyclerview-v7:22.2.0'
  compile 'com.android.support:cardview-v7:22.2.0'
  compile 'com.daimajia.swipelayout:library:1.2.0@aar'

  compile 'com.squareup.dagger:dagger:1.2.2'
  apt 'com.squareup.dagger:dagger-compiler:1.2.2'

  compile 'com.jakewharton:butterknife:6.1.0'
  compile 'com.jakewharton.timber:timber:3.0.2'

  compile 'io.reactivex:rxjava:1.0.10'
  compile 'io.reactivex:rxandroid:0.24.0'

  compile 'com.github.frankiesardo:auto-parcel:0.3'
  apt 'com.github.frankiesardo:auto-parcel-processor:0.3'

  compile 'com.squareup.sqlbrite:sqlbrite:0.1.0'
  compile 'de.psdev.licensesdialog:licensesdialog:1.7.0'
}
Configuration d’un module Gradle Android
Qu’avons-nous là ? D’abord, nous renseignons les plugins que nous voulons utiliser. Le plugin com.android.application est obligatoire dans les applications Android. C’est le plugin Gradle Android qui permet d’utiliser android { ... } et configurer votre application ou votre bibliothèque Android. Le plugin com.neenbedankt.android-apt est optionnel. Il est utilisé pour rajouter l’instruction apt dans les dépendances. Il ne sera pas expliqué dans le cadre de ce tutoriel. Pour avoir des informations supplémentaires au sujet de cette bibliothèque, n’hésitez pas à visiter son projet.

Une fois les plugins renseignés, vous pouvez enfin configurer votre projet Android ! Dès lors, il y a une chose importante à savoir. Avant Android Studio, et donc avant les projets Gradle, l’Android Manifest faisait foi. Toutes les informations renseignées dedans étaient la seule et quasi unique configuration de votre projet Android. Aujourd’hui, tout ce qui est renseigné dans android { ... } va écraser les données renseignées dans l’Android Manifest. Cela veut dire que si vous renseignez un numéro de version d’application différent dans l’Android Manifest et dans le fichier build, le plugin Gradle va écraser la valeur dans le manifest avec la valeur renseignée dans le build. Si vous êtes un ancien développeur Android, ne vous faites pas avoir !

Rappel : Les plugins renseignés sont disponibles grâce aux classpaths dans le fichier build à la racine du projet.

Les informations renseignées dans le bloc android { ... } sont simples à comprendre :

La propriété compileSdkVersion spécifie la version de l’API Android que vous utiliserez pour compiler votre application.
La propriété buildToolsVersion spécifie la version du Android SDK Build-tools souhaitée. Il est préférable de mettre à jour régulièrement cette information pour la dernière version installable depuis le SDK Manager.
Le bloc defaultConfig { ... } spécifie la configuration que vous allez surcharger dans le fichier manifest de votre application. Vous ne pouvez pas y renseigner toutes les permissions nécessaires à votre application, ni les écrans à travers les activités. Par contre, vous devez renseigner l’identifiant unique de votre application (applicationId), la version minimale de l’API Android que vous supportez (minSdkVersion), la cible de l’API Android (targetSdkVersion), la version interne de votre application (versionCode) et sa version externe (versionName).
Le bloc buildTypes { ... } spécifie comment compiler et empaqueter votre application dans différents modes, release ou debug par exemple. Sachant que dans notre exemple, nous laissons la configuration par défaut qui demande à votre environnement de travail de ne pas minifier le code source de l’application ni d’obfusquer l’application avec Proguard.
Nous appelons une version interne d’une application, le code qui est utilisé par le système Android et non visible à vos utilisateurs. Quant à la version externe, il s’agit du code visible auprès de vos utilisateurs soit dans le Play Store, soit dans les informations de l’application, soit dans votre application si vous souhaitez l’afficher. Cette dernière version peut prendre n’importe quelle valeur mais tentez de conserver une certaine convention dans son utilisation. Cela peut être une indication utile pour vous ou vos utilisateurs.

Nous retournons dans une configuration standard d’un projet Gradle avec le bloc dependencies { ... }. Comme son nom l’indique, vous y placerez toutes les dépendances de votre projet. Pour ceux qui n’utilisent pas encore un gestionnaire de dépendances, il n’est plus nécessaire de télécharger ses exécutables et les placer dans un dossier libs. Il suffit de connaitre l’identifiant unique de la bibliothèque (accessible via des moteurs de recherche comme celui de Maven Central) et de les placer dans ce bloc. Gradle se chargera de télécharger l’exécutable sur votre poste de travail et de l’inclure à la compilation de votre projet.

Bien entendu, si pour une raison X ou Y, vous devez absolument renseigner un exécutable dans votre projet ou, plus courant, spécifier les sources d’un autre module dans votre projet, Gradle vous permet de le faire :

dependencies {
  // Module dependency
  compile project(":lib")

  // Remote binary dependency
  compile 'com.android.support:appcompat-v7:19.0.1'

  // Local binary dependency
  compile fileTree(dir: 'libs', include: ['*.jar'])
}
Possibilité de déclaration des dépendances
Confort d'utilisation

Android Studio apporte un confort d’utilisation non négligeable face à son concurrent direct, ADT Eclipse. En plus de bénéficier de toutes les fonctionnalités d’IntelliJ IDEA en terme de refactoring et de personnalisation, Android Studio facilite la création des ressources et des composants. Impossible de toutes les énumérer, mais certaines de ces fonctionnalités sont utiles à connaitre. Vous êtes susceptible de les utiliser, tout comme moi, à chaque fois que vous développerez avec cet IDE.

Tout développeur se doit de maitriser son environnement de travail. Dans ce tutoriel, vous allez connaitre l’existence de quelques fonctionnalités sympathiques mais je vous encourage vivement à l’explorer d’avantage et à connaitre les raccourcis pour refactoriser votre code. Vous allez fortement gagner en productivité.

La barre d’outils
Toolbar d'Android Studio avec ses raccourcis spécifiques pour Android
Toolbar d’Android Studio avec ses raccourcis spécifiques pour Android
La barre d’outils regroupe des raccourcis utiles que vous serez amené à utiliser régulièrement. L’encadré bleu n’est pas spécifique à Android Studio, il est présent aussi dans IntelliJ IDEA. Ces raccourcis permettent d’exécuter des configurations d’exécution que vous avez soigneusement préparé ou que Android Studio aura créé pour vous. Par exemple, Android Studio crée automatiquement la configuration pour exécuter votre application sur votre terminal ou votre émulateur. Le menu affiche toutes les configurations à exécuter, la flèche verte lance l’exécution et le petit insecte lance l’exécution en mode debugger.

Nous n’allons pas apprendre à utiliser le mode debugger dans ce tutoriel mais je voulais le mentionner pour que vous sachez qu’il existe. Bien avant que les IDE modernes existent, la manière la plus répandue de trouver des bugs dans son code était d’imprimer sur la sortie standard des données. Aujourd’hui, des debuggers existent dans tous les IDE et permettent d’arrêter l’exécution d’un programme à des points stratégiques que vous aurez indiqué à Android Studio. Je vous encourage vivement à en savoir plus sur ce mode via la documentation officielle de IntelliJ. La maitrise de ce mode vous fera grandement gagner en productivité et vous ne pourrez plus vous en passer après.

L’encadré rouge regroupe les seuls raccourcis spécifiques à Android Studio et donc à Android. Le premier en partant de la gauche force la re-synchronisation de vos dépendances au niveau Gradle. Le second lance Android Virtual Device Manager pour consulter tous vos émulateurs. Le troisième vous permet d’améliorer votre SDK en téléchargeant de nouvelles versions d’API Android ou des bibliothèques signées de Google. Pour finir, le dernier lance Android Device Monitor pour consulter des métriques sur votre application comme sa consommation Internet ou l’utilisation de la mémoire. Ces métriques sont poussées mais peuvent être utiles quand vous faites attention à la qualité de vos applications. Vous l’aurez compris, consultez-les de temps en temps et analysez-les pour améliorer votre code.

Onglet Project
Passer de la vue Project à la vue Android
Passer de la vue Project à la vue Android
Situé à gauche du logiciel, un onglet "Project" permet la visualisation de tous les fichiers présents dans votre projet organisés selon la vue sélectionnée. Par défaut, la vue est "Project". Il en existe 8 différentes, à l’heure où sont écrites ces lignes, mais l’une d’elle est spécifique à Android Studio et a été conçue pour améliorer la gestion des fichiers des projets Android.

Sélectionnez la vue "Android" pour voir apparaitre tous vos modules, ici seulement app, et tous vos scripts Gradle. Dans chaque module, vous avez un accès simple et rapide à votre fichier Android Manifest, à vos sources Java et à vos ressources. Quant aux scripts Gradle, vous avez un accès direct à tous les fichiers build. Cette section regroupe toute la configuration de votre projet et de vos modules. N’hésitez pas à consulter l’information entre parenthèse pour connaitre la provenance du fichier.

C’est maintenant à vous de décider de la vue que vous préférez !

Créer une nouvelle ressource dans votre projet
Créer une nouvelle ressource dans votre projet
Les plus anciens se souviendront du site Android Asset Studio. Un petit site développé par Roman Nurik, designer chez Google, qui facilitait considérablement la création de ressources dans toutes les tailles demandées par Android. Pour rappel, il est nécessaire de fournir une même ressource en plusieurs tailles pour que Android puisse récupérer la ressource adéquate en fonction de l’appareil sur lequel s’exécute l’application (smartphone, tablette, montre, etc.).

Avec Android Studio, vous restez dans votre environnement de travail. Pour créer une ressource dans toutes les tailles, cliquez droit sur le dossier res, puis New > Image Asset et une fenêtre s’ouvrira pour créer votre ressource.

Créer de nouveaux composants dans votre projet
Créer de nouveaux composants dans votre projet
Pareil pour les composants du type activité, fragment ou des vues puisque vous pouvez générer des composants entiers. Par exemple, il arrive régulièrement d’avoir des écrans avec une liste dedans. Cliquez droit sur votre package, puis New > Fragment > Fragment (List) et Android Studio vous génère le fragment, son fichier XML et les autres ressources utiles si nécessaire.

Code source Java et ressources XML
Consulter les fichiers XML attachés à une Activity, un Fragment ou une vue
Consulter les fichiers XML attachés à une Activity, un Fragment ou une vue
A partir de n’importe quelle activité, fragment ou vue, vous pouvez accéder à tous les layout associés sur le côté gauche de votre code source au niveau de la déclaration de classe. Une petite icône, qui représente un fichier balisé, est cliquable et affiche une liste de tous ces fichiers XML. Cette petite colonne à gauche de votre code source renferme pas mal d’autres raccourcis. Par exemple, si vous accédez à une chaine de caractères déclarée dans un fichier XML, un raccourci vous permettra de vous y rendre. Il en est de même pour les couleurs et d’autres ressources du même genre.

Vue par défaut de vos interfaces dans votre fichier XML
Vue par défaut de vos interfaces dans votre fichier XML
Dernière fonctionnalité présente depuis les débuts d’Android Studio mais toujours aussi utile, c’est la prévisualisation de vos layout. Lorsque vous éditez un layout, un onglet à droite, nommé "Preview", permet la visualisation en direct de votre interface dans un terminal de votre choix, dans l’orientation de votre choix, avec le thème de votre choix, dans l’activité hôte de votre choix, dans la langue de votre choix et dans la version API Android de votre choix. Absolument tout est paramétrable et le rendu est le plus fidèle possible au rendu final sur un véritable téléphone.

Mais, cerise sur le gâteau, Android Studio permet aussi la prévisualisation sur plusieurs tailles différentes, plusieurs versions d’API Android différente et ainsi, en un coup d’oeil, détecter toutes les imperfections et/ou les incompatibilités de vos interfaces suivant tous ces paramètres. Cette prévisualiation est essentielle dans la confection de vos interfaces, abusez en jusqu’à obtenir quelque chose de correct sur le plus de téléphones différents.

Les vues XML pour vos interfaces sont nombreuses et pleines de surprise !