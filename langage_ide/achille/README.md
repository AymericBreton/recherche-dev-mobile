# Hey there 👋
FocusFox is a **simple and smart** free online editor with background sounds to help you *focus* on what you need to. It uses markdown features to quickly add formatting to plain text.
​
### Features 🦊
-  Sound library to help you focus 🔉 👉
-  Save and load functionality 💾
-  Spell checker for ~~misstakes~~
-  Markdown shortcuts
-  Focus writing mode ✍️
-  Dark mode 🌙