On utilise des IDE pour : 
- Pour leur rapidité :
Dans un environnement de développement intégré, les divers utilitaires ne nécessitent en général aucun réglage ni aucune intégration lors du processus de configuration. Les développeurs peuvent ainsi lancer rapidement le développement de nouvelles applications. Ils ne perdent pas non plus de temps à prendre en main chaque outil individuellement, car tous les utilitaires sont réunis dans une interface unique. Il s'agit d'un avantage non négligeable pour les nouveaux développeurs qui peuvent s'appuyer sur un IDE pour apprendre à utiliser les outils et les workflows standard de l'équipe qu'ils viennent d'intégrer. La plupart des fonctions d'un IDE font en fait gagner du temps, à l'instar de la saisie intelligente du code et de la génération automatique du code, qui permettent d'éviter la saisie manuelle de chaînes complètes de caractères.

- Pour leur facilité :
D'autres fonctions fréquemment disponibles dans les IDE aident les développeurs à organiser leur workflow et à résoudre les problèmes. Les environnements de développement intégré analysent le code au cours de sa rédaction et identifient ainsi en temps réel les bogues qui résultent d'erreurs humaines. Étant donné que tous les utilitaires sont rassemblés dans une seule interface graphique, les développeurs peuvent exécuter plusieurs actions sans avoir à basculer entre diverses applications. La plupart des IDE offrent aussi une fonction de coloration syntaxique avec des repères visuels qui mettent en évidence les structures syntaxiques du langage dans l'éditeur de texte. Certains IDE comprennent en plus des navigateurs de classes et d'objets, ainsi que des diagrammes hiérarchiques de classes pour certains langages.

Plusieurs IDE à utiliser pour un développement mobile :
- Netbeans
- Android Studio (Uniquement Android)
- Windev Mobile
- Xcode (Uniquement iOS)
