# Les IDE 👾
## **Un environnement de développement intégré, ou IDE, qu'est-ce que c'est ?** 👀

Un environnement de développement intégré, ou IDE, est un logiciel de création d'applications, qui rassemble des outils de développement fréquemment utilisés dans une seule interface utilisateur graphique (GUI). Un IDE se compose habituellement des éléments suivants :

* Éditeur de code source : un éditeur de texte qui aide à la rédaction du code logiciel, avec des fonctions telles que la coloration syntaxique avec repères visuels, la saisie automatique en fonction du langage et la vérification de bogues dans le code pendant la rédaction

* Utilitaires d'automatisation de version locale : des utilitaires qui permettent d'automatiser des tâches simples et reproductibles lors de la création d'une version locale du logiciel à destination du développeur lui-même, par exemple la compilation du code source en code binaire, la mise en paquet du code binaire et l'exécution de tests automatisés

* Débogueur : un programme qui permet de tester d'autres programmes en affichant l'emplacement des bogues dans le code d'origine

## Pourquoi les développeurs utilisent-ils des IDE ? 🔍
### Pour leur rapidité 🚀

Dans un environnement de développement intégré, les divers utilitaires ne nécessitent en général aucun réglage ni aucune intégration lors du processus de configuration. Les développeurs peuvent ainsi lancer rapidement le développement de nouvelles applications. Ils ne perdent pas non plus de temps à prendre en main chaque outil individuellement, car tous les utilitaires sont réunis dans une interface unique. Il s'agit d'un avantage non négligeable pour les nouveaux développeurs qui peuvent s'appuyer sur un IDE pour apprendre à utiliser les outils et les workflows standard de l'équipe qu'ils viennent d'intégrer. La plupart des fonctions d'un IDE font en fait gagner du temps, à l'instar de la saisie intelligente du code et de la génération automatique du code, qui permettent d'éviter la saisie manuelle de chaînes complètes de caractères.

### Pour leur facilité ✔

D'autres fonctions fréquemment disponibles dans les IDE aident les développeurs à organiser leur workflow et à résoudre les problèmes. Les environnements de développement intégré analysent le code au cours de sa rédaction et identifient ainsi en temps réel les bogues qui résultent d'erreurs humaines. Étant donné que tous les utilitaires sont rassemblés dans une seule interface graphique, les développeurs peuvent exécuter plusieurs actions sans avoir à basculer entre diverses applications. La plupart des IDE offrent aussi une fonction de coloration syntaxique avec des repères visuels qui mettent en évidence les structures syntaxiques du langage dans l'éditeur de texte. Certains IDE comprennent en plus des navigateurs de classes et d'objets, ainsi que des diagrammes hiérarchiques de classes pour certains langages.

### Les IDE sont-ils indispensables ? 🔥

Il est tout à fait possible de développer des applications sans environnement de développement intégré, ou, pour n'importe quel développeur, de créer son propre IDE en intégrant manuellement différents utilitaires à un éditeur de texte léger, tel que Vim ou Emacs. Certains développeurs apprécient les nombreuses possibilités de personnalisation et le niveau de contrôle qu'offre cette approche. En entreprise, cependant, le gain de temps, la standardisation de l'environnement et les fonctions d'automatisation des IDE modernes l'emportent généralement sur les autres considérations.

Aujourd'hui, la plupart des équipes de développement d'entreprise choisissent un IDE préconfiguré, en fonction de leur utilisation spécifique. Elles ne se demandent donc plus s'il faut adopter ou non un IDE, mais quel IDE il faut choisir.

### Types d'IDE fréquemment utilisés ❤

Il existe de nombreux cas d'utilisation techniques et métier pour les IDE, ce qui signifie qu'il existe de nombreux IDE propriétaires et Open Source sur le marché. Voici les critères qui permettent généralement de différencier les IDE :

* Nombre de langages pris en charge : certains IDE sont réservés à un langage et représentent donc le meilleur choix pour un modèle de programmation spécifique. IntelliJ, par exemple, est principalement un IDE Java. D'autres IDE prennent en charge une grande variété de langages. Par exemple, l'IDE Eclipse prend en charge les langages Java, XML, Python, etc.

* Systèmes d'exploitation pris en charge : le système d'exploitation du développeur limite le choix de l'IDE (sauf s'il s'agit d'un IDE cloud). De même, si l'application est développée pour un système d'exploitation spécifique (tel qu'Android ou iOS), le choix de l'IDE peut être limité.

* Fonctions d'automatisation : si la plupart des IDE comprennent les trois fonctions clés (édition de texte, automatisation des versions et débogage), de nombreux IDE comprennent également des fonctions supplémentaires telles que le réusinage de code, la recherche de code et les outils d'intégration et de déploiement continus (CI/CD).

* Effets sur les performances du système : l'empreinte mémoire d'un IDE peut être un élément important à prendre en compte si un développeur souhaite exécuter d'autres applications qui consomment de la mémoire en parallèle.

* Plug-ins et extensions : certains IDE permettent de personnaliser les workflows pour les adapter aux besoins et aux préférences du développeur. 

### IDE de développement mobile 🤖

Quasiment tous les secteurs ont été affectés par l'utilisation croissante des applications conçues pour les smartphones et les tablettes. De nombreuses entreprises se sont ainsi tournées vers le développement d'applications mobiles en plus des applications web traditionnelles. Le choix de la plateforme est un des facteurs clés du développement d'applications mobiles. Par exemple, si une nouvelle application doit pouvoir être utilisée sur un appareil iOS, Android, ainsi que sur une page web, il sera sans doute plus judicieux de commencer avec un IDE capable de prendre en charge plusieurs plateformes pour différents systèmes d'exploitation. 